package com.wbg.flow.utils.network



import com.wbg.flow.models.response.request.WeatherDataRequest

sealed class ApiType {
     data class WeatherDataByCity(val request: WeatherDataRequest):ApiType()
}