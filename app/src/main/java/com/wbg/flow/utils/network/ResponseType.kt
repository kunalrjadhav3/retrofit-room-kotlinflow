package com.wbg.flow.utils.network

sealed class ResponseType{
    data class Response(val response: Any):ResponseType()
    data class Error(val message:String):ResponseType()
    data class ErrorCode(val code:Int):ResponseType()
}