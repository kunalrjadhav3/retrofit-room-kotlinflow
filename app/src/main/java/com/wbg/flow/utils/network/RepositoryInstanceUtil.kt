package com.wbg.flow.utils.network

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.kunal.roomcoroutinesflow.room.Student
import com.example.kunal.roomcoroutinesflow.room.StudentsDao
import com.google.gson.GsonBuilder

import com.wbg.flow.retrofit.APIInterface
import com.wbg.flow.utils.Constants
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object RepositoryInstanceUtil {
    private var apiInstance: APIInterface?=null

    fun getApiInterface(): APIInterface {
        if (apiInstance == null) {
            val okHttpClient = OkHttpClient.Builder()
                .writeTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build()


            val retrofit = Retrofit.Builder().baseUrl(Constants.BASE_URL)
                //.addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
                .client(okHttpClient)
                .build()
            apiInstance = retrofit.create(APIInterface::class.java)
        }
        return apiInstance!!

    }

    @Database(entities = [Student::class], version = 1)
   open abstract class AppDB : RoomDatabase() {
        abstract fun studentsDao(): StudentsDao

        companion object {
            var INSTANCE: AppDB? = null

            fun getAppDataBase(context: Context): AppDB? {
                if (INSTANCE == null){
                    synchronized(AppDB::class){
                        INSTANCE = Room.databaseBuilder(context.applicationContext, AppDB::class.java, "myDB").build()
                    }
                }
                return INSTANCE
            }

            fun destroyDataBase(){
                INSTANCE = null
            }
        }
    }


}