package com.wbg.flow.repositories

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wbg.flow.MainApp
import com.wbg.flow.R
import com.wbg.flow.models.response.TwoCitiesResponse
import com.wbg.flow.models.response.WeatherResponse
import com.wbg.flow.models.response.request.WeatherDataRequest
import com.wbg.flow.models.response.response.RetrofitErrorResponse
import com.wbg.flow.retrofit.APIInterface
import com.wbg.flow.utils.Constants
import com.wbg.flow.utils.network.ResponseType

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.io.IOException

import java.net.SocketTimeoutException
import java.net.UnknownHostException


class Networkrepository(val apiInterface: APIInterface) {

    @Throws(Exception::class)
    private fun getRetrofitError(errorBody: String): String {
        val gson = Gson()
        val type = object : TypeToken<RetrofitErrorResponse>() {}.type
        val errorResponse: RetrofitErrorResponse? = gson.fromJson(errorBody, type)
        return errorResponse?.errors?.get(0)?.details
            ?: MainApp.appContext.getString(R.string.TechnicalError)

    }

    private fun handleError(e: Throwable): ResponseType.Error = when (e) {
        is SocketTimeoutException -> ResponseType.Error(MainApp.appContext.getString(R.string.ConnectionTimeout))
        is UnknownHostException -> ResponseType.Error(MainApp.appContext.getString(R.string.InternetCheck))
        is IOException -> ResponseType.Error(MainApp.appContext.getString(R.string.Timeout))
        else -> ResponseType.Error(e.toString())
    }


    private fun <T> handleResponse(response: Response<T>): ResponseType? {
        var responseType: ResponseType? = null
        try {
            if (response.isSuccessful && response.code().toString().equals(
                    MainApp.appContext.getString(
                        R.string.HTTP200
                    )
                )
            ) {
                response.body()?.let {
                    when (it) {
                        is String -> {
                            responseType = ResponseType.Response(it)
                        }
                        is WeatherResponse -> {
                            responseType = ResponseType.Response(it)
                        }

                    }
                }

            } else if (response.code().toString().equals(MainApp.appContext.getString(R.string.HTTP401))) {
                responseType = ResponseType.Error(response.code().toString())

            } else if (response.errorBody() != null) {
                response.errorBody()?.let {
                    responseType = ResponseType.Error(getRetrofitError(it.string() as String))
                }

            } else if (!response.message().isNullOrBlank()) {
                responseType = ResponseType.Error(response.message())
            }
        } catch (e: Exception) {
            responseType = ResponseType.Error(MainApp.appContext.getString(R.string.TechnicalError))
        }


        return responseType
    }

    internal suspend fun callWeatherDetailsZip(callback: (ResponseType) -> Unit) =
        withContext(Dispatchers.IO) {
            val responseOne =
                getFlow(apiInterface.getWeatherDetailsOfCity("Mumbai", Constants.WeatherApiApiKey))
            val responseTwo =
                getFlow(apiInterface.getWeatherDetailsOfCity("Doha", Constants.WeatherApiApiKey))
            responseOne.zip(responseTwo) { t1, t2 ->
                TwoCitiesResponse(t1.body()!!, t2.body()!!)
            }.onCompletion {
                it
            }
                .catch {
                    callback(handleError(it))
                }.collect {
                    callback(ResponseType.Response(it))
                }
        }


    internal suspend fun callWeatherDetailsConcatMap(callback: (ResponseType) -> Unit) =
        withContext(Dispatchers.IO) {
            val responseOne =
                getFlow(apiInterface.getWeatherDetailsOfCity("Mumbai", Constants.WeatherApiApiKey))
            val responseTwo =
                getFlow(apiInterface.getWeatherDetailsOfCity("Doha", Constants.WeatherApiApiKey))
            responseOne.flatMapConcat {
                responseTwo
            }.onCompletion {
                it
            }.catch {
                callback(handleError(it))
            }.collect {
                callback(handleResponse(it)!!)
            }
        }

  internal  suspend fun callWeatherDetailsOfCity(
        request: WeatherDataRequest,
        callback: (ResponseType) -> Unit
    ) = withContext(Dispatchers.IO) {

        getFlow(apiInterface.getWeatherDetailsOfCity(request.city, Constants.WeatherApiApiKey))
            .onStart { }
            .onCompletion { }
            .catch {
                callback(handleError(it))
            }.collect {
                callback(handleResponse(it)!!)

            }

    }


    private fun <T> getFlow(input: T): Flow<T> {
        return flow { emit(input) }
    }


}