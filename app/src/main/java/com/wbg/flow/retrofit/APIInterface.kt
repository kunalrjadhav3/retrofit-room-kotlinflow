package com.wbg.flow.retrofit


import com.wbg.flow.models.response.WeatherResponse
import retrofit2.Response
import retrofit2.http.*

interface APIInterface {
    @GET("/data/2.5/weather")
   suspend fun getWeatherDetailsOfCity(@Query("q") city:String, @Query("appid") appid:String):Response<WeatherResponse>
}