package com.wbg.flow.models.response.response

data class RetrofitError(
    val cause: String,
    val code: String,
    val details: String,
    //val eventId: String,
    val message: String,
    val uuid: String
)