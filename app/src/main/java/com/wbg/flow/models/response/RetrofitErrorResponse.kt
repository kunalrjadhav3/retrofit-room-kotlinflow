package com.wbg.flow.models.response.response

data class  RetrofitErrorResponse(
    val errors: List<RetrofitError>
)