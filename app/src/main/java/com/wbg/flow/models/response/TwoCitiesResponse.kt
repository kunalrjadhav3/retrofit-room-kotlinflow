package com.wbg.flow.models.response

data class TwoCitiesResponse(val cityOneResponse:WeatherResponse,val cityTwoResponse:WeatherResponse)