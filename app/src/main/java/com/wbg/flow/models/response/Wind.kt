package com.wbg.flow.models.response

data class Wind(
    val deg: Int,
    val speed: Double
)