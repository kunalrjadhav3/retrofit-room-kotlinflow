package com.wbg.flow.views.activities


import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.wbg.flow.R
import com.wbg.flow.models.response.request.WeatherDataRequest

import com.wbg.flow.utils.network.ApiType
import com.wbg.flow.utils.network.RepositoryInstanceUtil
import com.wbg.flow.viewmodels.MainActivityVM
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    // master
    private val viewModel: MainActivityVM by lazy {
        ViewModelProviders.of(this).get(MainActivityVM::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initApiObservers()
        initDBObservers()
        setOnClickListeners()


    }

   private fun setOnClickListeners() {
        btn_apiflow.setOnClickListener {
            viewModel.callApi(
                RepositoryInstanceUtil.getApiInterface(), ApiType.WeatherDataByCity(
                    WeatherDataRequest("Mumbai")
                )
            )
        }
        btn_dbflow.setOnClickListener {
            viewModel.observeDB(RepositoryInstanceUtil.AppDB.getAppDataBase(this)!!)
            viewModel.addStudentsToDB(RepositoryInstanceUtil.AppDB.getAppDataBase(this)!!)
        }
    }


    private fun initApiObservers() {
        viewModel.weatherResponseLiveData.observe(this, Observer {
            Toast.makeText(applicationContext, it.toString(), Toast.LENGTH_SHORT).show()
        })
        viewModel.zipWeatherResponseLiveData.observe(this, Observer {
            it
        })
        viewModel.responseErrorData.observe(this, Observer {
            Toast.makeText(applicationContext, it.toString(), Toast.LENGTH_SHORT).show()
        })
    }

   private fun initDBObservers() {
        viewModel.studentsListLiveData.observe(this, Observer {
            Toast.makeText(applicationContext, "List --> ${it}", Toast.LENGTH_SHORT).show()
        })

        viewModel.studentsLiveData.observe(this, Observer {
            Toast.makeText(applicationContext, "Student --> ${it}", Toast.LENGTH_SHORT).show()
        })
    }

}










