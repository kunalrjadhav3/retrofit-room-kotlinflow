package com.wbg.flow

import android.app.Application
import android.content.Context

class MainApp :Application() {
    companion object{
        lateinit var appContext: Context
    }

    override fun onCreate() {
        super.onCreate()
        appContext =this
    }

}