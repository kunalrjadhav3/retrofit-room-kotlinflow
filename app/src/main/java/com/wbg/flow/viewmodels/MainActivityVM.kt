package com.wbg.flow.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.kunal.roomcoroutinesflow.room.Student
import com.wbg.flow.models.response.TwoCitiesResponse
import com.wbg.flow.models.response.WeatherResponse

import com.wbg.flow.repositories.Networkrepository
import com.wbg.flow.retrofit.APIInterface
import com.wbg.flow.utils.network.ApiType
import com.wbg.flow.utils.network.RepositoryInstanceUtil
import com.wbg.flow.utils.network.ResponseType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MainActivityVM:BaseViewModel() {


    val studentsListLiveData by lazy { MutableLiveData<List<Student>>() }
    val studentsLiveData by lazy { MutableLiveData<Student>() }
    val responseErrorData by lazy { MutableLiveData<ResponseType.Error>() }
    val weatherResponseLiveData by lazy { MutableLiveData<WeatherResponse>() }
    val zipWeatherResponseLiveData by lazy { MutableLiveData<TwoCitiesResponse>() }



   internal fun callApi(apiInterface: APIInterface, apiType: Any) {
        viewModelScope.launch {
            val networkrepository = Networkrepository(apiInterface)

            when (apiType) {
                is ApiType.WeatherDataByCity->{
                networkrepository.callWeatherDetailsOfCity(apiType.request){
                    when(it){
                        is ResponseType.Response->weatherResponseLiveData.postValue(it.response as WeatherResponse)
                        is ResponseType.Error->responseErrorData.postValue(it)
                    }
                }
                   /* networkrepository.callWeatherDetailsZip{
                        when(it){
                            is ResponseType.Response->zipWeatherResponseLiveData.postValue(it.response as TwoCitiesResponse)
                            is ResponseType.Error->responseErrorData.postValue(it)
                        }
                    }
                    networkrepository.callWeatherDetailsConcatMap{
                        when(it){
                            is ResponseType.Response->weatherResponseLiveData.postValue(it.response as WeatherResponse)
                            is ResponseType.Error->responseErrorData.postValue(it)
                        }
                    }*/

                }
            }

        }
    }

   internal fun addStudentsToDB(db: RepositoryInstanceUtil.AppDB){
        viewModelScope.launch(Dispatchers.IO) {
            val list= mutableListOf<Student>()
            for(i in 1..2)
                list.add(Student(name = "Rock$i"))
            db.studentsDao().setAllStudents(list)
        }
    }

    internal fun addStudentToDB(db: RepositoryInstanceUtil.AppDB){
        viewModelScope.launch(Dispatchers.IO) {
            db.studentsDao().setStudent(Student(name = "Rock"))
        }
    }

    internal fun observeDB(db: RepositoryInstanceUtil.AppDB){
        viewModelScope.launch {
            db.studentsDao().getAllStudents().collect {
                if(it.isNotEmpty())
                    studentsListLiveData.postValue(it)

            }

            /*db.studentsDao().getAllStudentsDistinctUntilChanged("Rock").collect {
                    studentsLiveData.postValue(it)

            }*/
        }

    }
}