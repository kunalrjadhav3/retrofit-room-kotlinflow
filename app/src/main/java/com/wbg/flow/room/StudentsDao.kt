package com.example.kunal.roomcoroutinesflow.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged


/*
* Don't TAG functions with suspend keyword, for unit testing
* */

@Dao
interface StudentsDao {
    @Query("SELECT * FROM Student")
    fun getAllStudents(): Flow<List<Student>>

    @Query("SELECT * FROM Student WHERE name = :name")
    fun getStudent(name: String): Flow<Student>

    fun getAllStudentsDistinctUntilChanged(name: String) =
        getStudent(name).distinctUntilChanged()


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setAllStudents(shows: List<Student>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setStudent(student: Student)


}