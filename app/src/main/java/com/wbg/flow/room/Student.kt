package com.example.kunal.roomcoroutinesflow.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Student(
    @PrimaryKey(autoGenerate = true) val rollno: Int = 0,
    val name: String
)