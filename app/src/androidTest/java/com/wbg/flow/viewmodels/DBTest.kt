package com.wbg.flow.viewmodels

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.kunal.roomcoroutinesflow.room.Student
import com.wbg.flow.utils.network.RepositoryInstanceUtil
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DBTest {
    private lateinit var database: RepositoryInstanceUtil.AppDB
    private val testDispatcher = TestCoroutineDispatcher()
    private val testScope = TestCoroutineScope(testDispatcher)


    @Before
    fun setup() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database =
            Room.inMemoryDatabaseBuilder(context, RepositoryInstanceUtil.AppDB::class.java).build()
    }

    @Test
    fun test_getAllStudents() {
        val studentList = mutableListOf<Student>()
        studentList.add(Student(name = "Rock"))
        testScope.launch(testDispatcher) {
            database.studentsDao().setAllStudents(studentList)
            database.studentsDao().getAllStudents().collect {
                assertEquals(it.get(0).name, studentList.get(0).name)
            }
        }

    }

    @Test
    fun test_getStudent() {
        val student = Student(name = "Rock")
        testScope.launch(testDispatcher) {
            database.studentsDao().setStudent(student)
            database.studentsDao().getStudent(student.name).collect {
                assertEquals(it.name, student.name)
            }
        }

    }


    @After
    fun cleanup() {
        testDispatcher.cleanupTestCoroutines()
        database.clearAllTables()
        database.close()
    }

}