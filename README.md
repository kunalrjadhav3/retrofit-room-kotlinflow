
### Description ###

This project is used to showcase capabilities of Kotlin coroutines and flow.

* Make asynchronous network API calls with Retrofit library with coroutines and flow
* Store data in local database using ROOM persistence library with coroutines and flow
* A demonstrated example of unit testing ROOM-coroutines-flow setup
